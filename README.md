Main two measurement modules are: legibility-count, and perform-benchmark

legibility-count outputs to the console per evaluation method:
	- Total characters
	- Total number of keywords and identifiers
	- Total number of operators
	
perform-benchmark uses benchmark.js to evaluate the speed of each evaluation 
method under three different scenarios:
	1. Low complexity branching
	2. High complexity branching
	3. Complex conditions
