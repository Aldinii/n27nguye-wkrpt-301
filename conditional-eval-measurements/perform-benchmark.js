const fs = require('fs');
const path = require('path');
const Benchmark = require('benchmark');

// Note: Switch and Map must be commented out to benchmark scenario 3
const scenarioMethods = require('./scenario-1');
const {
  ifElse,
  switchCase,
  ternary,
  mapMethod,
  queueMethod,
} = scenarioMethods;

// Context Setup
const suite = new Benchmark.Suite();
const resultsFile = path.join(__dirname, 'scenario-1-results.txt');

suite
  .add('ifElse', () => ifElse())
  .add('Switch', () => switchCase())
  .add('Ternary', () => ternary())
  .add('Map', () => mapMethod())
  .add('Queue', () => queueMethod())
  .on('start', () => {
    fs.appendFile(resultsFile, '\n\nTest Results:', err => {
      if (err) throw err;
    });
  })
  .on('cycle', event => {
    const name = String(event.target.name);
    const meanTime = String(event.target.stats.mean * Math.pow(10, 9)) + ' ns';
    const performanceLog = `${name}: ${meanTime}`;
    console.log(performanceLog);
    fs.appendFile(resultsFile, `\n${performanceLog}`, err => {
      if (err) throw err;
      console.log('Result written to file');
    });
  })
  .on('complete', function() {
    console.log('Benchmarking done!');
  })
  .run({ async: true });
