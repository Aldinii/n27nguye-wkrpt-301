const ifElse = () => {
  if (2 > 3 || 1 < 0) {
    return false;
  } else {
    return true;
  }
};

const ternary = () => (2 > 3 || 1 < 0 ? false : true);

const queueMethod = () => {
  const queue = [() => (2 > 3 || 1 < 0 ? outputError() : false)];

  const passingEvaluator = queue.find(evaluator => evaluator() !== false);

  const evaluatorResult = passingEvaluator && passingEvaluator();

  return evaluatorResult || true;
};

module.exports = {
  ifElse,
  ternary,
  queueMethod,
};
