const R = require('ramda');

const methodStringsObj = require('./method-strings-scenario-1');
const methodStrings = Object.values(methodStringsObj);

/*
  Function: 
    splitMethodStrings
  Description: 
    Takes in a set of strings, and maps those strings to
    an array containing each line of those strings
  
  Example Input:
    methodStrings: [
      `if (condition) {
        return true;
      }`
    ]
  Example Output: [
      'if (condition) {',
      ' return true;',
      '}'
    ]
*/
const splitMethodStrings = methodStrings =>
  methodStrings.map(methodString => methodString.split('\n'));

/*
  Function: 
    countTotalChars
  Description: 
    Takes in a string which had been split, and outputs
    how many characters they have in total
  
  Example Input:
    splitString: [
      'if (condition) {',
      ' return true;',
      '}'
    ]
  Example Output:
    31
*/
const countTotalChars = splitString =>
  splitString.reduce((acc, string) => acc + string.length, 0);

/*
  Function: 
    countTotalCharsOfMethodStrings
  Description: 
    Takes in a set of strings, and outputs, per string, how
    many characters they have
  
  Example Input:
    methodStrings: [
      `if (condition) {
        return true;
      }`,

      `return condition ? true : false;`
    ]
  Example Output: [
      31,
      32
    ]
*/
const countTotalCharsOfMethodStrings = methodStrings =>
  splitMethodStrings(methodStrings).map(methodStringSplit =>
    countTotalChars(methodStringSplit),
  );

// Prints the result to the programmer console
const totalCharsOutputArray = R.zip(
  Object.keys(methodStringsObj),
  countTotalCharsOfMethodStrings(methodStrings),
).map(tuple => `${tuple[0]}: ${tuple[1]}`);

console.log('Total Number of Characters per Evaluation Method:');
totalCharsOutputArray.forEach(output => console.log(output));
console.log('');

const keyWords = [
  'if',
  'else if',
  'else',
  'switch',
  'case',
  'default',
  'break',
  'const',
  'false',
  'return',
];
const operators = ['\\s=\\s', '===', '\\?', '\\|\\|', '&&', '!==', ':'];
const identifiers = [
  'input',
  'performAction',
  'map',
  'queue',
  'find',
  'passingEvaluator',
  'evaluatorResult',
];

const wordSet = [keyWords, identifiers, operators];

/*
  Function: 
    countWordInstances
  Description: 
    Takes in a string, and a set of words to count, and outputs
    the number of times words in that set show up in the string
  
  Example Input:
    string: 'He has five fingers and five toes'
    wordsToCount: ['five', 'fingers']
  Example Output:
    3
*/
const countWordInstances = (string, wordsToCount) =>
  wordsToCount.reduce((acc, wordToCount) => {
    const query = new RegExp(wordToCount, 'g');
    const matchResult = string.match(query);
    return matchResult ? acc + matchResult.length : acc;
  }, 0);

/*
  Function: 
    countTotalWordInstances
  Description: 
    Performs the same actions as countWordInstances, but takes
    in a set of a set of words instead
  
  Example Input:
    string: 'Rex, woof, is, woof, a, woof, dog'
    wordSet: [
      ['Rex', 'dog'],
      ['woof']
    ]
  Example Output:
    5
*/
const countTotalWordInstances = (string, wordSet) =>
  wordSet.reduce((acc, set) => acc + countWordInstances(string, set), 0);

/*
  Function: 
    countWordInstancesPerString
  Description: 
    Performs the same actions as countTotalWordInstances, but takes in a set
    of strings instead
  
  Example Input:
    strings: [
      'Rex, woof, is, woof, a, woof, dog',
      'Mittens is a cat, not a dog, and does not say woof'
    ]
    wordSet: [
      ['Rex', 'dog'],
      ['woof']
    ]
  Example Output:
    [
      5,
      2
    ]
*/
const countWordInstancesPerString = (strings, wordSet) =>
  strings.map(string => countTotalWordInstances(string, wordSet));

// Prints the result to the programmer console
const totalKeyWordsAndIdentifiersOutputArray = R.zip(
  Object.keys(methodStringsObj),
  countWordInstancesPerString(methodStrings, wordSet),
).map(tuple => `${tuple[0]}: ${tuple[1]}`);

console.log(
  'Total Number of Key Words, Identifiers, and Operators per Evaluation Method:',
);
totalKeyWordsAndIdentifiersOutputArray.forEach(output => console.log(output));
console.log('');
