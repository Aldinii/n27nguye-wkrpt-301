// Context Setup
const input = 'value';

// Methods
const ifElse = () => {
  if (input === 'value') {
    return true;
  } else {
    return false;
  }
};

const switchCase = () => {
  switch (input) {
    case 'value':
      return true;
      break;
    default:
      return false;
      break;
  }
};

const ternary = () => (input === 'value' ? true : false);

const mapMethod = () => {
  const map = {
    value: true,
  };

  return map[input] || false;
};

const queueMethod = () => {
  const queue = [input => (input === 'value' ? true : false)];

  const passingEvaluator = queue.find(evaluator => evaluator(input));

  const evaluatorResult = passingEvaluator && passingEvaluator(input);

  return evaluatorResult || false;
};

module.exports = {
  ifElse,
  switchCase,
  ternary,
  mapMethod,
  queueMethod,
};
