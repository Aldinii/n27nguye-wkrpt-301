const input = '';

// Methods
const ifElse = `if (input === 'value1') {
  performAction1();
} else if (input === 'value2') {
  performAction2();
} else if (input === 'value3') {
  performAction3();
} else if (input === 'value4') {
  performAction4();
} else if (input === 'value5') {
  performAction5();
} else if (input === 'value6') {
  performAction6();
} else if (input === 'value7') {
  performAction7();
} else if (input === 'value8') {
  performAction8();
} else if (input === 'value9') {
  performAction9();
} else {
  return false;
}`;

const switchCase = `switch (input) {
  case 'value1':
    performAction1();
    break;
  case 'value2':
    performAction2();
    break;
  case 'value3':
    performAction3();
    break;
  case 'value4':
    performAction4();
    break;
  case 'value5':
    performAction5();
    break;
  case 'value6':
    performAction6();
    break;
  case 'value7':
    performAction7();
    break;
  case 'value8':
    performAction8();
    break;
  case 'value9':
    performAction9();
    break;
  default:
    return false;
    break;
}`;

const ternary = `return input === 'value1'
? performAction1()
: input === 'value2'
  ? performAction2()
  : input === 'value3'
    ? performAction3()
    : input === 'value4'
      ? performAction4()
      : input === 'value5'
        ? performAction5()
        : input === 'value6'
          ? performAction6()
          : input === 'value7'
            ? performAction7()
            : input === 'value8'
              ? performAction8()
              : input === 'value9' ? performAction9() : false;`;

const mapMethod = `const map = {
  value1: performAction1(),
  value2: performAction2(),
  value3: performAction3(),
  value4: performAction4(),
  value5: performAction5(),
  value6: performAction6(),
  value7: performAction7(),
  value8: performAction8(),
  value9: performAction9(),
};

return map[input] || false;`;

const queueMethod = `const queue = [
  input => (input === 'value1' ? performAction1() : false),
  input => (input === 'value2' ? performAction2() : false),
  input => (input === 'value3' ? performAction3() : false),
  input => (input === 'value4' ? performAction4() : false),
  input => (input === 'value5' ? performAction5() : false),
  input => (input === 'value6' ? performAction6() : false),
  input => (input === 'value7' ? performAction7() : false),
  input => (input === 'value8' ? performAction8() : false),
  input => (input === 'value9' ? performAction9() : false),
];

const passingEvaluator = queue.find(evaluator => evaluator(input) !== false);

const evaluatorResult = passingEvaluator && passingEvaluator(input);

return evaluatorResult || false;`;

module.exports = {
  ifElse,
  switchCase,
  ternary,
  mapMethod,
  queueMethod,
};
