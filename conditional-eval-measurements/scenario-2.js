// Context Setup
const input = 'value';

// Methods
const ifElse = () => {
  if (false) {
    // Do nothing
  } else if (false) {
    // Do nothing
  } else if (false) {
    // Do nothing
  } else if (false) {
    // Do nothing
  } else if (false) {
    // Do nothing
  } else if (false) {
    // Do nothing
  } else if (false) {
    // Do nothing
  } else if (false) {
    // Do nothing
  } else if (false) {
    // Do nothing
  } else {
    return false;
  }
};

const switchCase = () => {
  switch (input) {
    case null:
    case null:
    case null:
    case null:
    case null:
    case null:
    case null:
    case null:
    case null:
      break;
    default:
      return false;
      break;
  }
};

const ternary = () =>
  false
    ? true
    : false
      ? true
      : false
        ? true
        : false
          ? true
          : false
            ? true
            : false ? true : false ? true : false ? true : false ? true : false;

const mapMethod = () => {
  const map = {
    '1': true,
    '2': true,
    '3': true,
    '4': true,
    '5': true,
    '6': true,
    '7': true,
    '8': true,
    '9': true,
  };

  return map[input] || false;
};

const queueMethod = () => {
  const queue = [
    input => (false ? true : false),
    input => (false ? true : false),
    input => (false ? true : false),
    input => (false ? true : false),
    input => (false ? true : false),
    input => (false ? true : false),
    input => (false ? true : false),
    input => (false ? true : false),
    input => (false ? true : false),
  ];

  const passingEvaluator = queue.find(evaluator => evaluator(input));

  const evaluatorResult = passingEvaluator && passingEvaluator(input);

  return evaluatorResult || false;
};

module.exports = {
  ifElse,
  switchCase,
  ternary,
  mapMethod,
  queueMethod,
};
