const ifElse = `if ( (two > three) || (one < zero) ) {
  outputError();
} else {
  return true;
}`;

const ternary = `return (two > three) || (one < zero) ? outputError() : true;`;

const queueMethod = `const queue = [
  ({zero, one, two, three}) => ((two > three) || (one < zero) ? outputError() : false),
];

const passingEvaluator = queue.find(evaluator => evaluator(input) !== false);

const evaluatorResult = passingEvaluator && passingEvaluator(input);

return evaluatorResult || true;`;

module.exports = { ifElse, ternary, queueMethod };
