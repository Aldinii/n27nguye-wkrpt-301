const ifElseString = `if (input === 'value') {
    performAction();
  } else {
    return false;
  }`;

const switchCaseString = `switch (input) {
    case 'value':
      performAction();
      break;
    default:
      return false;
      break;
  }`;

const ternaryString = `return input === 'value' ? performAction() : false`;

const mapString = `const map = {
    value: performAction(),
  };
  
  return map[input] && map[input]() || false;`;

const queueString = `const queue = [input => (input === 'value' ? performAction() : false)];
  
  const passingEvaluator = queue.find(evaluator => evaluator(input) !== false);
  
  const evaluatorResult = passingEvaluator && passingEvaluator(input);
  
  return evaluatorResult || false;`;

module.exports = {
  ifElseString,
  switchCaseString,
  ternaryString,
  mapString,
  queueString,
};
